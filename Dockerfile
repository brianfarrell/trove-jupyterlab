# syntax=docker/dockerfile:1

FROM jupyter/tensorflow-notebook:latest

LABEL fm.trove.image.name="Jupyter tensorflow-notebook Enhanced"
LABEL fm.trove.image.authors="Brian Farrell <brian.farrell@me.com>"

USER ${NB_UID}

# Install Python 3 packages
RUN conda install --quiet --yes \
    ipython-sql \
    jupyter-book \
    lxml \
    mplfinance \
    myst-parser \
    nasdaq-data-link \
    numpy-financial \
    pandas-datareader \
    pgspecial \
    psycopg2 \
    python-dotenv \
    quantlib \
    redis-py \
    statsmodels

RUN conda install --quiet --yes -c pyviz \
    pyviz_comms \
    panel \
    hvplot \
    holoviews \
    bokeh \
    datashader \
    param \
    colorcet

RUN conda install --quiet --yes -c btf pymbs

RUN pip install openpyxl

RUN conda clean --all -f -y && \
    fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}"

WORKDIR "${HOME}"
